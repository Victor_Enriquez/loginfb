var express = require('express');
var router = express.Router();
var rest = require('../mymodules/rest');
var User = require('../models/users');

router.get('/',function(req,res,next){
	res.render('signin',{title:'Equipo 5'});
});

router.post('/loginFB',function(req,res,next){
  var redirect_uri = "http://localhost:3000/facebook_redirect"
  res.redirect("https://www.facebook.com/v2.10/dialog/oauth?client_id=1136026253195221&redirect_uri="+redirect_uri);
});

router.get('/facebook_redirect',function(req,res,next){
  //capturar datos de facebook
  	var redirect_uri = "http://localhost:3000/facebook_redirect";
  	var facebook_code = req.query.code;
  	var accesstoken_call = {
  	    host: 'graph.facebook.com',
  	    port: 443,
  	    path: '/v2.10/oauth/access_token?client_id=1136026253195221&redirect_uri='+redirect_uri+
  	    '&client_secret=926add611effd76906a1fb2375ebc75e&code='+facebook_code,
  	    method: 'GET',
  	    headers: {
  	        'Content-Type': 'application/json'
  	    }
  	};

  	rest.getJSON(accesstoken_call, function(statusCode, access_token_response) {
  	    var FB_path = '/me?fields=id,name,email,picture&access_token='+access_token_response.access_token;
  	    console.log("token: " + access_token_response.access_token);
  	    console.log("FB_path: " + FB_path);
  	    var userinfo_call = {
  		    host: 'graph.facebook.com',
  		    port: 443,
  		    path: FB_path,
  		    method: 'GET',
  		    headers: {
  		        'Content-Type': 'application/json'
  		    }
  		};
  		rest.getJSON(userinfo_call, function(statusCode, user_info_response) {
  	    	console.log("USUARIO ID :::::::::::::::: " + user_info_response.name);
  			User.find( {where: { name : user_info_response.name  }} ).then(function(user){ //facebookID en tabla de usuarios y facebook access token
  				//user.facebook_access_token = access_token_response.access_token;

     var newUser = {
         "email":user_info_response.email ,
         "name":user_info_response.name ,
         "username":"",
         "password":access_token_response.access_token,
         "address":"",
         "status":"active"
     };

     User.build(newUser).save().then(function(){
       console.log("LISTO");
       res.json(newUser);
        });

  	});
  });
  });
});

  router.get('/createuser', function(req, res, next) {
    var query = {"email":req.body.email,"name":req.body.name, "username":req.body.username, "password":req.body.password,"address":req.body.address};
    //User.save(query);
    res.json(query);
  });

module.exports = router;
