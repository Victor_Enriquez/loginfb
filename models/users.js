var Sequelize = require('sequelize');

var mysql_host = process.env.MYSQLHOST;
if(mysql_host == undefined || mysql_host == ""){
    mysql_host = 'localhost';
}
var mysql_pass = process.env.MYSQLPASS;
if(mysql_pass == undefined || mysql_host == ""){
    mysql_pass = '';
}

var sequelize = new Sequelize('fblogin', 'root', mysql_pass, {
  host: mysql_host,
  dialect: 'mysql'
});

sequelize.Promise = global.Promise;

var User = sequelize.define('users', {
    "id": { type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    "name": { type: Sequelize.STRING },
    "password" : { type: Sequelize.STRING },
    "address" : { type: Sequelize.STRING },
    "created_at" : { type: Sequelize.DATE },
    "status" : { type: Sequelize.STRING,  defaultValue: Sequelize.NOW }
});
User.sync();
module.exports = User;
