var express = require('express');
var app = express();
var expressHbs = require('express-handlebars');
var sigin = require('./routes/index');


// view engine setup
app.engine('.hbs', expressHbs({defaultLayout: 'layout', extname: '.hbs'}));
app.set('view engine', 'hbs');

app.use('/',sigin);

module.exports = app;
